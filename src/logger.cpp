#include "logger.h"
#include <iostream>

#include <QDate>
#include <QDir>
#include <QTextStream>
#include <QTime>

Logger &Logger::getLogger()
{
    static Logger instance;
    return instance;
}

void Logger::writeLog(const std::string &logHeader, QString &&errorCode, QString &&content, std::mutex *m)
{
    QFile logFile(QDir::currentPath() + "/" + QString::fromStdString(filename));
    QTextStream stream(&logFile);

    if (!logFile.open(QIODevice::ReadWrite | QIODevice::Append))
    {
        QTextStream(stdout) << "Cannot open file" << Qt::endl;
        return;
    }
    if (m != nullptr)
        m->lock();
    stream << QDate::currentDate().toString(Qt::ISODate) + " " + QTime::currentTime().toString() + " ";
    stream.flush();
    auto search = setOfErrors.find(errorCode);
    if (search != setOfErrors.end())
    {
        stream << "[ " + errorCode + " ] " + QString::fromStdString(logHeader) + content << Qt::endl
               << Qt::endl;
        stream.flush();
    }
    else
    {
        stream << "Not known error code" << Qt::endl;
    }
    if (m != nullptr)
        m->unlock();
    logFile.close();
}
