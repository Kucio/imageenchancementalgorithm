#include "fuzzy_logic.h"

float EPSILON = 1e-10f;

template<typename T>
bool closeEnough(T a, T b)
{
    return fabs(a - b) < EPSILON;
}

template <typename Ty>
int32_t sgn(Ty value)
{
    return (Ty(0) < value) - (value < Ty(0));
}

void contrastEnchancementMethod(Mat &input, unsigned int height, unsigned int width,
                                double *gammaMin, double *gammaMax)
{
    double meanResult = -1.0;
    auto middleHeight = static_cast<int>(height / 2);
    auto middleWidth = static_cast<int>(width / 2);
    auto orig = static_cast<double>(
        input.at<uchar>(
            static_cast<int>(ceil(middleHeight)),
            static_cast<int>(ceil(middleWidth))));

    //mean
    Scalar meanVal = mean(input);
    meanResult = static_cast<double>(meanVal.val[0]);

    //find lMin and lMax
    double lMin, lMax;
    minMaxLoc(input, &lMin, &lMax);

    double gamma = 0.0;
    if (gammaMin && gammaMax)
        gamma = (*gammaMin + (*gammaMax - *gammaMin) *
                                 ((orig - lMin) / (lMax - lMin)));
    else if (gammaMin && !gammaMax)
        gamma = *gammaMin;

    //Author method
    auto diff = (orig - meanResult) / 255;
    auto sig = sgn(diff);
    auto enchance = fabs(pow(fabs(diff), gamma));
    auto tempResult = meanResult + 255 * sig * enchance;
    double finalResult = round(tempResult);
    if (closeEnough(0.0, finalResult) || finalResult < 0.0)
        finalResult = 0.0;
    if (closeEnough(255.0, finalResult) || finalResult > 255.0)
        finalResult = 255.0;
    input.at<uchar>(ceil(middleHeight),
                    ceil(middleWidth)) = static_cast<uchar>(finalResult);
}

void cutHistogram(Mat &histogram, unsigned int &aParam, unsigned int &cParam,
                  Size size, unsigned int &lMin, unsigned int &lMax,
                  QTextStream &inputStream)
{
    int tempSize = size.height * size.width; //total size of all
                                             // symbols in an image
    inputStream << "Whole size: " << tempSize << " pixels" << '\n' << Qt::flush;
    unsigned int pixelsToCut = static_cast<unsigned int>(floor(tempSize * 0.01));  // 2 percent cut, because cuttings is for 1 percent darkest and 1 percent brightest pixels
    unsigned int tempPixelBottom = 0;                                              // temporary variable to hold information
                                                                                   // about how many pixels had been processed in loop
    unsigned int tempPixelUp = 0;                                                  // temporary variable to hold information
                                                                                   // about how many pixels had been processed in loop
    bool foundPixelToCutBottom = false;
    bool foundPixelToCutUp = false;
    bool foundLMin = false;
    bool foundLMax = false;

    for (int i = 0; i < 256; i++) //skip first and last 10 values
    {
        if (foundPixelToCutBottom)
            break;
        unsigned int occurs = static_cast<unsigned int>(
            histogram.at<float>(0, i)); //the number of times
                                                          //number has occured
        if (occurs > 0 && foundLMin == false)
        {
            lMin = i;
            inputStream << "LMin = " << lMin << '\n' << Qt::flush;
            foundLMin = true;
        }
        tempPixelBottom += occurs;
        if (tempPixelBottom >= pixelsToCut)
        {
            inputStream << "Pixel to cut at bottom: " << i << '\n' << Qt::flush;
            aParam = i;
            // cut histogram to found values
            for (int j = aParam; j > 0; --j)
            {
                if (static_cast<unsigned int>(j) != aParam)
                {
                    auto temp = histogram.at<float>(0, j);
                    histogram.at<float>(0, j) = 0;
                    histogram.at<float>(0, static_cast<int>(aParam)) += temp;
                }
            }
            foundPixelToCutBottom = true;
        }
    }
    for (int i = 255; i >= 0; --i)
    {
        if (foundPixelToCutUp)
            break;
        float occurs = histogram.at<float>(0, i); //the number of times a
                                                  //sybmol has occured
        if (occurs > 0 && foundLMax == false)
        {
            lMax = static_cast<unsigned int>(i);
            inputStream << "LMax = " << lMax << '\n' << Qt::flush;
            foundLMax = true;
        }

        tempPixelUp += occurs;
        if (tempPixelUp >= pixelsToCut)
        {
            inputStream << "Pixel to cut from up: " << i << '\n' << Qt::flush;
            cParam = static_cast<unsigned int>(i);
            for (int k = cParam + 1; k < 256; k++)
            {
                auto temp = histogram.at<float>(0, k);
                histogram.at<float>(0, k) = 0;
                histogram.at<float>(0, static_cast<int>(cParam)) += temp;
            }
            foundPixelToCutUp = true;
        }
    }
}

void applyHistogram(Mat &channel, unsigned int aParam, unsigned int cParam,
                    QTextStream &inputStream)
{
    inputStream << "Input A: " << aParam << " "
                << "Input C: "
                << cParam << '\n' << Qt::flush;
    for (int i = 0; i < channel.rows; i++)
    {
        for (int j = 0; j < channel.cols; j++)
        {
            auto compare_val = static_cast<unsigned int>(channel.at<uchar>(i, j));
            if (compare_val <= aParam)
            {
                channel.at<uchar>(i, j) = static_cast<uchar>(aParam);
                continue;
            }
            if (compare_val >= cParam)
            {
                channel.at<uchar>(i, j) = static_cast<uchar>(cParam);
                continue;
            }
        }
    }
}

Mat fuzzification(Mat input, unsigned int &cParam, unsigned int &aParam,
                  unsigned int &bParam, float &maxEntropy,
                  Size size, QTextStream &inputStream)
{
    Mat finalFuzzyImage(input.size(), CV_32FC1);

    int tempSize = size.height * size.width; //total size of all symbols
                                             //in an image
    for (unsigned int b = aParam + 1; b < cParam; b++)
    {
        Mat fuzzyImage(input.size(), CV_32FC1);
        fuzzyImage = cv::Mat::zeros(input.rows, input.cols, CV_32FC1);

        for (int i = 0; i < input.rows; ++i)
        {
            for (int j = 0; j < input.cols; ++j)
            {
                fuzzyImage.at<float>(i, j) =
                    static_cast<float>(input.at<uchar>(i, j));
            }
        }
        double summaryEntropy = 0;
        double imageEntropy = 0;
        for (int i = 0; i < fuzzyImage.rows; i++)
        {
            for (int j = 0; j < fuzzyImage.cols; j++)
            {
                double val = static_cast<double>(fuzzyImage.at<float>(i, j));
                double pixelEntropy = 0.0;
                if (val <= aParam)
                    val = 0;
                else if (val > aParam && (val < b))
                    val = (pow((val - aParam), 2)) /
                          ((b - aParam) * (cParam - aParam));
                else if (val >= b && (val <= cParam))
                    val = 1 - ((pow((cParam - val), 2)) /
                               ((cParam - b) * (cParam - aParam)));
                else if (val > cParam)
                    val = 1;
                else
                    abort();

                if ((val >= (1 - EPSILON)) || (val <= (0 + EPSILON)))
                    pixelEntropy = 0;
                else
                {
                    pixelEntropy = -(val * log2(val)) - ((1 - val) *
                                                         log2(1 - val));
                }
                fuzzyImage.at<float>(i, j) = static_cast<float>(val);
                summaryEntropy += pixelEntropy;
            }
        }
        imageEntropy = summaryEntropy / tempSize;
        if (imageEntropy > static_cast<double>(maxEntropy))
        {
            bParam = b;
            maxEntropy = static_cast<float>(imageEntropy);
            finalFuzzyImage.release();
            finalFuzzyImage = fuzzyImage.clone();
        }
    }

    inputStream << "Final params: B: " << bParam
                << " Max Entropy: " << maxEntropy << '\n' << Qt::flush;
    return finalFuzzyImage;
}

Mat enlarge(Mat input, unsigned int type,
            int width, int height)
{
    //enlarge to prevent from edge effect
    Mat in = input.clone();
    Mat enlarged((in.rows + (height)), (in.cols + (width)),
                 static_cast<int>(CV_8UC(type)));
    Mat imgPanelRoi(enlarged, Rect(width / 2, height / 2, in.cols, in.rows));
    in.copyTo(imgPanelRoi);

    //Left
    Rect recLeft(0, 0, width / 2, in.rows);
    Mat roiLeft = in(recLeft);
    Rect WhereRecLeft(0, height / 2, roiLeft.cols, roiLeft.rows);
    roiLeft.copyTo(enlarged(WhereRecLeft));
    //Right
    Rect recRight(in.cols - (width / 2), 0, width / 2, in.rows);
    Mat roiRight = in(recRight);
    Rect WhereRecRight(in.cols + (width / 2), (height / 2),
                       roiRight.cols, roiRight.rows);
    roiRight.copyTo(enlarged(WhereRecRight));
    //Up
    Rect recUp(0, 0, in.cols, height / 2);
    Mat roiUp = in(recUp);
    Rect WhereRecUp(width / 2, 0, roiUp.cols, roiUp.rows);
    roiUp.copyTo(enlarged(WhereRecUp));
    //Down
    Rect recDown(0, in.rows - (height / 2), in.cols, (height / 2));
    Mat roiDown = in(recDown);
    Rect WhereRecDown(width / 2, in.rows + (height / 2), roiDown.cols, roiDown.rows);
    roiDown.copyTo(enlarged(WhereRecDown));
    //Diagonal
    //
    Rect recLeftUp(0, 0, width / 2, height / 2);
    Mat roiLeftUp = in(recLeftUp);
    Rect WhereRecLeftUp(0, 0, roiLeftUp.cols, roiLeftUp.rows);
    roiLeftUp.copyTo(enlarged(WhereRecLeftUp));
    //
    Rect recRightUp(in.cols - width / 2, 0, width / 2, height / 2);
    Mat roiRightUp = in(recRightUp);
    Rect WhereRecRightUp(in.cols + (width / 2), 0,
                         roiRightUp.cols, roiRightUp.rows);
    roiRightUp.copyTo(enlarged(WhereRecRightUp));
    //
    Rect recLeftDown(0, in.rows - height / 2, width / 2, height / 2);
    Mat roiLeftDown = in(recLeftDown);
    Rect WhereRecLeftDown(0, in.rows + (height / 2),
                          roiLeftDown.cols, roiLeftDown.rows);
    roiLeftDown.copyTo(enlarged(WhereRecLeftDown));
    //
    Rect recRightDown(in.cols - (width / 2), in.rows - (height / 2),
                      (width / 2), (height / 2));
    Mat roiRightDown = in(recRightDown);
    Rect WhereRecRightDown(in.cols + (width / 2), in.rows + (height / 2),
                           roiRightDown.cols, roiRightDown.rows);
    roiRightDown.copyTo(enlarged(WhereRecRightDown));
    return enlarged;
}

Mat normalizationAndFiltering(Mat input, int width,
                              int height,
                              double *gammaMin, double *gammaMax)
{
    using namespace std::placeholders; // for _1, _2, _3...
    Mat normalized = input.clone();

    for (int i = 0; i < input.rows; i++)
    {
        for (int j = 0; j < input.cols; j++)
        {
            normalized.at<float>(i, j) = floor(input.at<float>(i, j) * 255);
        }
    }

    normalized.convertTo(normalized, CV_8UC1);
    normalized = enlarge(normalized, 1, width, height);

    std::function<void(Mat &, unsigned int, unsigned int,
                       double *, double *)>
        enchanceMethod =
            contrastEnchancementMethod;
    for (int j = 0; j < normalized.rows - height; j++)
    {
        for (int i = 0; i < normalized.cols - width; i++)
        {
            Rect roi(i, j, width, height);
            Mat slidingWindow = normalized(roi);
            enchanceMethod(slidingWindow, slidingWindow.rows,
                           slidingWindow.cols, gammaMin, gammaMax);
        }
    }

    Mat newNormalized(normalized.size(), CV_32FC1);
    newNormalized = cv::Mat::zeros(normalized.rows, normalized.cols,
                                   CV_32FC1);
    for (int i = 0; i < normalized.rows; ++i)
    {
        for (int j = 0; j < normalized.cols; ++j)
        {
            newNormalized.at<float>(i, j) =
                static_cast<float>(normalized.at<uchar>(i, j));
        }
    }
    //cut back from enlarged
    Rect cutScene((width / 2), (height / 2), input.cols, input.rows);
    Mat final = newNormalized(cutScene);
    return final;
}

void defuzzification(Mat &input, unsigned int &cParam, unsigned int &aParam,
                     unsigned int &bParam, unsigned int lMin, unsigned int lMax)
{
    float border1 = bParam - aParam;
    float border2 = cParam - aParam;
    float border = border1 / border2;

    for (int i = 0; i < input.rows; i++)
    {
        for (int j = 0; j < input.cols; j++)
        {
            float val = input.at<float>(i, j);
            //Simple normalization:
            input.at<float>(i, j) = val / 255;
        }
    }

    for (int i = 0; i < input.rows; i++)
    {
        for (int j = 0; j < input.cols; j++)
        {
            float val = input.at<float>(i, j);
            if (closeEnough(val, 0.0f))
            {
                input.at<float>(i, j) = lMin;
            }
            else if (val > 0.0f + EPSILON && (val <= border))
            {
                input.at<float>(i, j) = floor(lMin +
                                              (((lMax - lMin) / (cParam - aParam)) * sqrt(val * (bParam - aParam) * (cParam - aParam))));
            }
            else if ((val > border) && (val <= 1.0f - EPSILON))
            {
                input.at<float>(i, j) = floor(
                    lMin + (((lMax - lMin) / (cParam - aParam)) * (cParam - aParam - sqrt((1 - val) * (cParam - bParam) * (cParam - aParam)))));
            }
            else if (val > 1.0f - EPSILON)
            {
                input.at<float>(i, j) = lMax;
            }
        }
    }
    input.convertTo(input, CV_8UC1);
}

std::pair<double, double> calculateContrast(Mat &histogram, Mat &sourceImage,
                                            unsigned int sizeOfHistogram)
{
    double average = 0;

    for (int i = 0; i < sourceImage.rows; i++)
    {
        for (int j = 0; j < sourceImage.cols; j++)
        {
            average += static_cast<double>(sourceImage.at<uchar>(i, j));
        }
    }

    int sizeOfImage = sourceImage.cols * sourceImage.rows;
    auto finalAverageOutput = average / sizeOfImage;

    //Output contrast
    double contrast = 0;
    float summaryHist = 0;
    double simpleSummary = 0;
    auto histSize = static_cast<int>(sizeOfHistogram);
    for (int i = 0; i < histSize; i++)
    {
        auto lMinusAvL = fabs(i - finalAverageOutput);
        auto normalizedHist =
            (static_cast<float>(histogram.at<float>(0, i) / sizeOfImage));
        summaryHist += normalizedHist;
        simpleSummary += static_cast<unsigned int>(histogram.at<float>(0, i));
        double value =
            fabs((2 * lMinusAvL + 255) - fabs(2 * lMinusAvL - 255)) * static_cast<double>(normalizedHist);
        contrast += value;
    }
    double finalOutputContrast = contrast / 510;
    auto result = std::make_pair(finalOutputContrast, finalAverageOutput);
    return result;
}
