#include "fuzzy_logic.h"
#include "logger.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <iostream>
#include <mutex>

#include <QTextStream>
#include <QDesktopWidget>
#include <QFileDialog>
#include <QMessageBox>
#include <QPixmap>
#include <QScreen>
#include <QStyle>

using namespace cv;

int MainWindow::sizeOfHistogram = 256;
float MainWindow::histogramRange[] = {0, 255};
const float *MainWindow::histRangeAsPointer = {histogramRange};
bool MainWindow::uni = true;
bool MainWindow::acc = false;

static std::mutex barrier;
static std::mutex logBarrier;

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),
                                          ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->AddaptiveContrastButton, &QPushButton::clicked, this, &MainWindow::runAdaptiveContrastEnchancement);
    connect(ui->CalcContrastButton, &QPushButton::clicked, this, &MainWindow::calcContrast);
    connect(ui->saveButton, &QPushButton::clicked, this, &MainWindow::saveToFile);
    connect(ui->quitButton, &QPushButton::clicked, this, &MainWindow::quit);

    auto width = 1150; //fixed width
    auto height = 600; //fixed height
    QSize fixSize(width, height);

    this->setGeometry(
        QStyle::alignedRect(
            Qt::LeftToRight,
            Qt::AlignCenter,
            fixSize,
            QGuiApplication::primaryScreen()->geometry()));
}

bool MainWindow::loadImage(QString &w, QString &h)
{
    ui->InputLabel->clear();
    ui->OutputLabel->clear();
    QMessageBox msgBox;
    if (w.toInt() == 0 || h.toInt() == 0)
    {
        msgBox.setText("Please provide input params");
        msgBox.exec();
        return false;
    }

    fileName = QFileDialog::getOpenFileName(this,
                                            tr("Open Image"),
                                            QDir::homePath(),
                                            tr("Image Files (*.png *.jpg *.jpeg *.bmp *.tif)"),
                                            nullptr,
                                            QFileDialog::DontUseNativeDialog);
    if (fileName.isEmpty())
    {
        return false;
    }
    input = imread(fileName.toStdString(), IMREAD_COLOR);
    Mat printImage = input.clone();
    if (input.empty())
    {
        msgBox.setText("Wrong image");
        msgBox.exec();
        return false;
    }
    if (input.cols > 1024 || input.rows > 1024)
    {
        msgBox.setText("Image cant be bigger than 1024x1024");
        msgBox.exec();
        return false;
    }
    if (fileName.contains(".tif") || fileName.contains(".tiff"))
    {
        ui->InputLabel->setText("Preview doesnt work for tif and tiff images \ndue to Qt limitations");
    }
    else
    {
        cvtColor(printImage, printImage, COLOR_BGR2RGB);
        ui->InputLabel->setPixmap(QPixmap::fromImage(
            QImage(printImage.data, printImage.cols,
                   printImage.rows,
                   static_cast<int>(printImage.step), QImage::Format_RGB888)));
    }

    return true;
}

void MainWindow::runAdaptiveContrastEnchancement()
{
    input.release();
    output.release();

    /*
     * RGBParameters[0] = fuzzy logic function parameters for blue channel
     * RGBParameters[1] = fuzzy logic function parameters for green channel
     * RGBParameters[2] = fuzzy logic function parameters for red channel
     */
    Parameters BGRParameters[3];

    QString windowWidth = ui->ACEWindowSizeW->text();
    QString windowHeight = ui->ACEWindowSizeH->text();

    bool status = loadImage(windowWidth, windowHeight);
    if (!status)
        return;
    Logger::getLogger().writeLog("", QString("message"), QString::fromStdString("Procedure for file " + fileName.toStdString() + " started"),
                                 &logBarrier);
    ui->progressBar->setValue(50); // 50% value to symbolize finished handling
                                   // of input image

    Mat colorSpaces[3];
    Mat histograms[3];
    Mat fuzzyDomainImages[3];
    Mat normalizedImages[3];
    std::thread workerThreads[3];
    Mat outputImage = input.clone();

    split(outputImage, colorSpaces);

    // Compute the histograms:
    calcHist(&colorSpaces[0], 1, nullptr, Mat(), histograms[0], 1,
             &sizeOfHistogram, &histRangeAsPointer, uni, acc);
    calcHist(&colorSpaces[1], 1, nullptr, Mat(), histograms[1], 1,
             &sizeOfHistogram, &histRangeAsPointer, uni, acc);
    calcHist(&colorSpaces[2], 1, nullptr, Mat(), histograms[2], 1,
             &sizeOfHistogram, &histRangeAsPointer, uni, acc);

    double tempGMin = ui->gMin->value();
    double tempGMax = ui->gMax->value();
    double *gMin = &tempGMin;
    double *gMax = &tempGMax;

    auto concurrentLaunch = [&](unsigned int i) {
        std::function<std::string()> currentChannel = [i, this]() -> std::string {
            std::string currentChannelName;
            if (i == static_cast<unsigned int>(Channel::Blue))
                currentChannelName = std::string("processing: " + enumToStringMap.find(Channel::Blue)->second + " channel\n");
            else if (i == static_cast<unsigned int>(Channel::Green))
                currentChannelName = std::string("processing: " + enumToStringMap.find(Channel::Green)->second + " channel\n");
            else if (i == static_cast<unsigned int>(Channel::Red))
                currentChannelName = std::string("processing: " + enumToStringMap.find(Channel::Red)->second + " channel\n");
            else
            {
                QMessageBox msgBox;
                msgBox.setText("Wrong channel number received");
                msgBox.exec();
                return std::string();
            }
            return currentChannelName;
        };
        //Locking a mutex to reveice proper image size and alocate
        //thread local stringstream
        barrier.lock();
        auto tempSize = input.size();
        if (ui->progressBar->value() <= 100)
            ui->progressBar->setValue(ui->progressBar->value() + 1);
        barrier.unlock();
        QString filename = QString::fromUtf8(Logger::getLogger().getFileName().c_str());
        QTextStream ss(&filename);
        cutHistogram(histograms[i], BGRParameters[i].a, BGRParameters[i].c,
                     tempSize, BGRParameters[i].lMin,
                     BGRParameters[i].lMax, ss);
        applyHistogram(colorSpaces[i], BGRParameters[i].a,
                       BGRParameters[i].c, ss);
        barrier.lock();
        if (ui->progressBar->value() <= 100)
            ui->progressBar->setValue(ui->progressBar->value() + 1);
        barrier.unlock();
        fuzzyDomainImages[i] = fuzzification(colorSpaces[i], BGRParameters[i].c,
                                             BGRParameters[i].a,
                                             BGRParameters[i].b,
                                             BGRParameters[i].maxEntropy,
                                             tempSize, ss);
        barrier.lock();
        if (ui->progressBar->value() <= 100)
            ui->progressBar->setValue(ui->progressBar->value() + 4);
        barrier.unlock();
        normalizedImages[i] = normalizationAndFiltering(fuzzyDomainImages[i],
                                                        windowWidth.toInt(),
                                                        windowHeight.toInt(), gMin, gMax);
        barrier.lock();
        if (ui->progressBar->value() <= 100)
            ui->progressBar->setValue(ui->progressBar->value() + 4);
        barrier.unlock();
        defuzzification(normalizedImages[i], BGRParameters[i].c,
                        BGRParameters[i].a, BGRParameters[i].b,
                        BGRParameters[i].lMin,
                        BGRParameters[i].lMax);
        colorSpaces[i] = normalizedImages[i];
        barrier.lock();
        if (ui->progressBar->value() <= 100)
            ui->progressBar->setValue(ui->progressBar->value() + 4);

        auto log = ss.readAll().toStdString();
        auto pos = log.find(Logger::getLogger().getFileName());
        if (pos != std::string::npos)
            log.erase(pos, Logger::getLogger().getFileName().length());

        Logger::getLogger().writeLog(currentChannel(), QString("message"),
                                     QString::fromStdString(log), &logBarrier);
        barrier.unlock();
    };

    std::atomic<unsigned int> threadWait{0};
    for (; threadWait < 3; ++threadWait)
    {
        barrier.lock();
        workerThreads[threadWait] =
            std::thread(concurrentLaunch,
                        static_cast<unsigned int>(threadWait));
        barrier.unlock();
    }
    //wait for workerThreads to finished
    for (auto &workerThread : workerThreads)
    {
        workerThread.join();
    }

    //Merge
    merge(colorSpaces, 3, outputImage);
    imwrite("tmp.jpg", outputImage);
    output = outputImage.clone();
    outputImg.load("tmp.jpg");
    QFile tempFile("tmp.jpg");
    ui->OutputLabel->setPixmap(QPixmap::fromImage(outputImg));
    tempFile.remove();
    ui->progressBar->setValue(100);
    Logger::getLogger().writeLog("", QString("message"),
                                 QString::fromStdString("Procedure for file " +
                                                        fileName.toStdString() + " ended\n\n"),
                                 &logBarrier);
}

void MainWindow::calcContrast()
{
    Mat colorSpacesIn[3];
    Mat colorSpacesOut[3];
    Mat histogramsIn[3];
    Mat histogramsOut[3];
    split(input, colorSpacesIn);
    split(output, colorSpacesOut);
    calcHist(&colorSpacesIn[0], 1, nullptr, Mat(), histogramsIn[0], 1, &sizeOfHistogram,
             &histRangeAsPointer, uni, acc);
    calcHist(&colorSpacesIn[1], 1, nullptr, Mat(), histogramsIn[1], 1, &sizeOfHistogram,
             &histRangeAsPointer, uni, acc);
    calcHist(&colorSpacesIn[2], 1, nullptr, Mat(), histogramsIn[2], 1, &sizeOfHistogram, &histRangeAsPointer,
             uni, acc);
    auto inputResBlue = calculateContrast(histogramsIn[0],
                                          colorSpacesIn[0], static_cast<unsigned int>(sizeOfHistogram));
    auto inputResGreen = calculateContrast(histogramsIn[1],
                                           colorSpacesIn[1], static_cast<unsigned int>(sizeOfHistogram));
    auto inputResRed = calculateContrast(histogramsIn[2],
                                         colorSpacesIn[2], static_cast<unsigned int>(sizeOfHistogram));

    calcHist(&colorSpacesOut[0], 1, nullptr, Mat(), histogramsOut[0], 1, &sizeOfHistogram,
             &histRangeAsPointer, uni, acc);
    calcHist(&colorSpacesOut[1], 1, nullptr, Mat(), histogramsOut[1], 1, &sizeOfHistogram,
             &histRangeAsPointer, uni, acc);
    calcHist(&colorSpacesOut[2], 1, nullptr, Mat(), histogramsOut[2], 1, &sizeOfHistogram,
             &histRangeAsPointer, uni, acc);
    auto outputResBlue = calculateContrast(histogramsOut[0],
                                           colorSpacesOut[0], static_cast<unsigned int>(sizeOfHistogram));
    auto outputResGreen = calculateContrast(histogramsOut[1],
                                            colorSpacesOut[1], static_cast<unsigned int>(sizeOfHistogram));
    auto outputResRed = calculateContrast(histogramsOut[2],
                                          colorSpacesOut[2], static_cast<unsigned int>(sizeOfHistogram));

    auto finalResultInput = std::pair<double, double>{
        (inputResBlue.first + inputResGreen.first + inputResRed.first) / 3,
        (inputResBlue.second + inputResGreen.second + inputResRed.second) / 3};
    auto finalResultOutput = std::pair<double, double>{
        (outputResBlue.first + outputResGreen.first + outputResRed.first) / 3,
        (outputResBlue.second + outputResGreen.second + outputResRed.second) / 3};

    ui->InContrast->setText(QString::number(finalResultInput.first));
    ui->OutContrast->setText(QString::number(finalResultOutput.first));
    ui->averIn->setText(QString::number(finalResultInput.second));
    ui->averOut->setText(QString::number(finalResultOutput.second));
}

void MainWindow::saveToFile()
{
    if (outputImg.isNull())
    {
        return;
    }
    QMessageBox msgBox;
    if (outputImg.save("output.jpg"))
    {
        msgBox.setText("Saved image successfully");
    }
    else
    {
        msgBox.setText("Failed to save image");
    }
    msgBox.exec();
}

void MainWindow::quit()
{
    this->close();
}

MainWindow::~MainWindow()
{
    delete ui;
}
