My refactored Master's Thesis. Image enchancement method based on fuzzy logic.

To compile this project gcc compatible with c++17 standard, Qt 5.9 and OpenCV 4.0.0 are needed.

After setup now it is possible to use cmake to compile it easily by typing from project directory (assuming that all needed dependencies had been met):

cmake . && make
