#pragma once

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <math.h>
#include <iostream>
#include <stdio.h>
#include <tgmath.h>
#include <iomanip>
#include <thread>
#include <vector>
#include <cstdlib>
#include <functional>
#include <utility>

#include <QTextStream>

using namespace cv;

void contrastEnchancementMethod(Mat &input, unsigned int height, unsigned int width,
                                double *gammaMin, double *gammaMax);

void cutHistogram(Mat &histogram, unsigned int &aParam, unsigned int &cParam,
                  Size size, unsigned int &lMin, unsigned int &lMax,
                  QTextStream &inputStream);

void applyHistogram(Mat &channel, unsigned int aParam, unsigned int cParam,
                    QTextStream &inputStream);

Mat fuzzification(Mat input, unsigned int &cParam, unsigned int &aParam,
                  unsigned int &bParam, float &maxEntropy,
                  Size size, QTextStream &inputStream);

Mat enlarge(Mat input, unsigned int type,
            int width, int height);

Mat normalizationAndFiltering(Mat input, int width,
                              int height,
                              double *gammaMin, double *gammaMax);

void defuzzification(Mat &input, unsigned int &cParam, unsigned int &aParam,
                     unsigned int &bParam, unsigned int lMin, unsigned int lMax);

std::pair<double, double> calculateContrast(Mat &histogram, Mat &sourceImage,
                                            unsigned int sizeOfHistogram);