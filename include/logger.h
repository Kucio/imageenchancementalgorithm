#pragma once

#include <fstream>
#include <set>
#include <mutex>

#include <QString>
#include <QFile>
class Logger
{
public:
    static Logger &getLogger();
    Logger(Logger const &) = delete;
    void operator=(Logger const &) = delete;
    void writeLog(const std::string &logHeader, QString &&errorCode, QString &&content, std::mutex *m = nullptr);
    const std::string& getFileName() { return filename; }
private:
    Logger() = default;
    /**
    * contains sets of acceptable error messages
    */
    std::set<QString> setOfErrors{"message", "warning", "error"};
    const std::string filename = "log.txt";
};
