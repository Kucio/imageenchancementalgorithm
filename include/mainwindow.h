#pragma once

#include "opencv2/core/mat.hpp"

#include <fstream>
#include <sstream>
#include <map>

#include <QMainWindow>
#include <QPushButton>

namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    enum class Channel : unsigned int
    {
        Blue = 0,
        Green = 1,
        Red = 2
    };
    std::map<Channel, std::string> enumToStringMap = {
        {Channel::Blue, "blue"}, {Channel::Green, "green"}, {Channel::Red, "red"}};

    /*struct parameters is representation of fuzzy logic parameters
      a,b,c - params of S-shaped activation function
      lMin and lMax - maximum and minimum value of grey-level in loadImage
      maxEntropy - biggest found entropy with S-shaped function usage */
    struct Parameters
    {
        unsigned int a = 0;
        unsigned int b = 0;
        unsigned int c = 0;
        unsigned int lMin = 0;
        unsigned int lMax = 0;
        float maxEntropy = 0.0f;
    };

    Ui::MainWindow *ui;
    QImage outputImg;
    QString fileName;
    cv::Mat output;
    cv::Mat input;
    bool loadImage(QString &w, QString &h);

    /* OpenCV params needed for histogram calculation */
    static int sizeOfHistogram;
    static float histogramRange[];
    static const float *histRangeAsPointer;
    static bool uni;
    static bool acc;

private slots:
    void runAdaptiveContrastEnchancement();
    void calcContrast();
    void saveToFile();
    void quit();
};
